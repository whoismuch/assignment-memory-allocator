//
// Created by Хумай Байрамова on 31.12.2021.
//

#include "test.h"

void *heap = NULL;

struct block_header *get_block_header(void *data) {
    return (struct block_header *) data;
}

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
    struct block_header, contents));
}

void *try_heap_init(size_t size) {
    debug("Инициализация кучи\n");
    heap = heap_init(size);
    if (heap == NULL) {
        err("Мы не смогли проинициализировать кучу\n\n");
    }
    debug("Успешно проинициализировали кучу!\n\n");
    return heap;
}

void test1() {
    debug("Тест 1 запустился (Обычное успешное выделение памяти)\n");

    debug_heap(stdout, heap);

    void *allocated_mem = _malloc(30);

    debug_heap(stdout, heap);

    if (!allocated_mem) {
        err("Тест 1 завален.\nУказатель на выделенный участок памяти равен NULL");
    }
    struct block_header *header = block_get_header(allocated_mem);
    if (header->is_free) {
        debug_heap(stdout, heap);
        err("Тест 1 завален.\nАллоцированная память свободна");
    }
    if (header->capacity.bytes != 30) {
        debug_heap(stdout, heap);
        err("Тест 1 завален.\nЗаполненность структуры не соотвествует требоваемой");
    }
    _free(allocated_mem);
    debug_heap(stdout, heap);
    debug("Тест 1 завершен успешно!!!\n\n\n");
}

void test2() {

    debug("Тест 2 запустился (Освобождение одного блока из нескольких выделенных)\n");

    debug_heap(stdout, heap);

    void *allocated_mem1 = _malloc(976);
    void *allocated_mem2 = _malloc(25);
    void *allocated_mem3 = _malloc(123);

    debug_heap(stdout, heap);

    struct block_header *header1 = block_get_header(allocated_mem1);
    struct block_header *header2 = block_get_header(allocated_mem2);
    struct block_header *header3 = block_get_header(allocated_mem3);


    _free(allocated_mem1);

    if (!allocated_mem1) {
        debug_heap(stdout, heap);
        err("Тест 2 завален.\nУказатель на выделенный участок памяти равен NULL");
    }


    if (!header1->is_free) {
        debug_heap(stdout, heap);
        err("Тест 2 завален.\nАллоцированная память занята, а должна быть свободна");
    }

    if (header2->is_free) {
        debug_heap(stdout, heap);
        err("Тест 2 завален.\nМы не освобождали второй блок памяти");
    }

    if (header3->is_free) {
        debug_heap(stdout, heap);
        err("Тест 2 завален.\nМы не освобождали третий блок памяти");
    }

    _free(allocated_mem2);
    _free(allocated_mem3);

    debug_heap(stdout, heap);

    debug("Тест 2 завершен успешно!!!\n\n\n");
}

void test3() {

    debug("Тест 3 запустился (Освобождение двух блоков из нескольких выделенных.)\n");

    debug_heap(stdout, heap);

    void *allocated_mem1 = _malloc(976);
    void *allocated_mem2 = _malloc(123);
    void *allocated_mem3 = _malloc(64);

    debug_heap(stdout, heap);

    struct block_header *header1 = block_get_header(allocated_mem1);
    struct block_header *header3 = block_get_header(allocated_mem3);

    _free(allocated_mem2);
    _free(allocated_mem1);


    if (!allocated_mem1 || !allocated_mem2) {
        debug_heap(stdout, heap);
        err("Тест 3 завален.\nУказатель на выделенные участки памяти равен NULL");
    }

    if (!header1->is_free) {
        debug_heap(stdout, heap);
        err("Тест 3 завален.\nАллоцированная память занята, а должна быть свободна");
    }

    if (header1->capacity.bytes != 976 + size_from_capacity((block_capacity) {.bytes = 123}).bytes) {
        debug_heap(stdout, heap);
        err("Тест 3 завален.\nПервые 2 блока должны были смерджиться, но не вышло");
    }

    if (header3->is_free) {
        debug_heap(stdout, heap);
        err("Тест 3 завален.\nМы не освобождали третий блок памяти");
    }

    _free(allocated_mem3);
    debug_heap(stdout, heap);
    debug("Тест 3 завершен успешно!!!\n\n\n");
}

void test4() {
    debug("Тест 4 запустился (Память закончилась, новый регион памяти расширяет старый.)\n");

    debug_heap(stdout, heap);

    void *allocated_mem1 = _malloc(8175);
    void *allocated_mem2 = _malloc(50);

    debug_heap(stdout, heap);

    if (!allocated_mem1 || !allocated_mem2) {
        debug_heap(stdout, heap);
        err("Тест 4 завален.\nУказатель на выделенные участки памяти равен NULL");
    }

    struct block_header *header1 = block_get_header(allocated_mem1);
    struct block_header *header2 = block_get_header(allocated_mem2);

    if ((void*) (header1->contents + header1->capacity.bytes) != (void*) header2) {
        debug_heap(stdout, heap);
        err("Тест 4 завален.\nНовый регион памяти расширяет старый.");
    }

    if (header1->capacity.bytes != 8175) {
        debug_heap(stdout, heap);
        err("Тест 4 завален.\nЗаполненность структуры 1 не соотвествует требоваемой");
    }

    if (header2->capacity.bytes != 50) {
        debug_heap(stdout, heap);
        err("Тест 4 завален.\nЗаполненность структуры 2 не соотвествует требоваемой");
    }

    _free(allocated_mem1);
    _free(allocated_mem2);

    debug_heap(stdout, heap);

    debug("Тест 4 завершен успешно!!!\n\n\n");
}

void test5() {
    debug("Тест 5 запустился (Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте. )\n");

    try_heap_init(8192);

    debug_heap(stdout, heap);

    void *allocated_mem1 = _malloc(8175);

    debug_heap(stdout, heap);

    if (!allocated_mem1) {
        err("Тест 5 завален.\nУказатель на выделенный участок памяти равен NULL");
    }

    struct block_header *header1 = block_get_header(allocated_mem1);

    while (header1->next) {
        header1 = header1->next;
    }

    void *allocated_mem2 = mmap((void *) (header1->contents + header1->capacity.bytes), 20000, PROT_READ | PROT_WRITE,
                                MAP_PRIVATE | MAP_ANONYMOUS , 0, 0);

    if (!allocated_mem2 || allocated_mem2 == MAP_FAILED) {
        debug_heap(stdout, heap);
        err("Тест 5 завален.\nНе удалось саллоцировать память\n");
    }

    void *allocated_mem3 = _malloc(50);
    struct block_header *header3 = block_get_header(allocated_mem3);

    if ((void*)( header1->contents + header1->capacity.bytes) == (void*) header3) {
        debug_heap(stdout, heap);
        err("Тест 5 завален.\nВторой запрашиваемый блок располагается прямо за первым, а должен был выделиться в другом месте\n");
    }

    _free(allocated_mem1);
    _free(allocated_mem2);
    _free(allocated_mem3);

    debug_heap(stdout, heap);

    debug("Тест 5 завершен успешно!!!\n\n\n");
}
