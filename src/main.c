//
// Created by Хумай Байрамова on 31.12.2021.
//

#include <stdlib.h>
#include "test.h"

const size_t INIT_HEAP_SIZE = 8192;

int main() {
    try_heap_init(INIT_HEAP_SIZE);
    debug("Начинаем тестирование\n");
    test1();
    test2();
    test3();
    test4();
    test5();
    debug("Все тесты успешно пройдены!\n");
    return 0;
}
