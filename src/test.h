//
// Created by Хумай Байрамова on 31.12.2021.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

#include <stdlib.h>
#include <stdio.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug(const char *fmt, ...);

void* try_heap_init(size_t size);

void test1();

void test2();

void test3();

void test4();

void test5();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
